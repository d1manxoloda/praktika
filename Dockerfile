FROM python:3.6

WORKDIR  /code
COPY . /code

RUN pip install --verbose -r requirements.txt

ENTRYPOINT [ "python3" ]
CMD ["app.py"]

EXPOSE 5000